import json
from dataclasses import asdict, dataclass

import valkey
from rich import print, print_json


def my_handler(msg):
    print("my_handler:")
    print(msg)
    print(msg["data"])


r = valkey.Valkey()
print(r)

# p0 = r.pubsub()
# p0.subscribe(
#     "my-first-channel",
#     "my-second-channel",
# )

p1 = r.pubsub(ignore_subscribe_messages=True)
p1.subscribe(
    **{
        "handler-channel": my_handler,
    },
)

# print(p0.channels)
print(p1.channels)


# r.publish("my-first-channel", "some data")
# m = p0.get_message()
# print(m)


@dataclass
class Event:
    key0: str


e = Event(**{"key0": "some data"})
print(e)
e_json = json.dumps(asdict(e))

print_json(e_json)

r.publish("handler-channel", e_json)

# m = p1.get_message()
# m = p1.get_message()
# m = p1.get_message()
# print(m)

for m in p1.listen():
    print(m)
