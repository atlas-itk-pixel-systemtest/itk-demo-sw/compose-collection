# Containerized git (alpine-git)

## Why

Since CC7 comes with a very old version of Git and a newer version is not easy
to install everywhere (need root and/or third-party repos, or compile from source)
and option is to run it in a container.

## How

To install a `git` shim to your path run

```shell
. ./setup
```

This will let you run the latest git using Docker as if it was installed locally.


