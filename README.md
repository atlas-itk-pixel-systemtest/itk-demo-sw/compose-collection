# DeMi compose.yaml Collection

## What is it

Curated list of compose files for third-party containerized applications
useful with DeMi.

This should contain only apps that do not need an extra image build because
a functional image is already available from elsewhere.

Extra layers like using a Dockerfile to bake in some configuration 
should only be added if another solution is not practical.

## User Guide

- Clone repo
- Using Dockge (**recommended**). 
  1. In `dockge/compose.yaml` point `$DOCKGE_STACKS_DIR` at `compose-collection`.
  1. Start Dockge. Use to start the other stacks from it's web UI.

- Modify the compose.yaml file(s) to your needs
- From CLI: If available you can use shims, shell functions or aliases to make the app(s) available in the shell transparently.

## Maintenance Tools

There is a Python project `compose_collection`  with some tools to work with this collection.
