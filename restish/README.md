# Restish container
The restish container allows you to access the openAPI HTTP endpoints of other microservices via your command line.
## Usage
After starting the container you can access it via the ./restish.sh script. This script will allow you to execute any restish command. For example if you want to access the health endpoint of the configdb microservice you can do the following:
```bash
./restish configdb health
```
## Available microservices
On startup the restish container will automatically discover all microservices that are available in the docker network. You can update and see this list of all available microservices by executing the following command:
```bash
./restish update
```