from service_registry import ServiceRegistry

FILE_PATH = "/home/demi/.restish/apis.json"


def update():
    sr = ServiceRegistry()

    apis = sr.get_apis("demi")
    names = []

    # apis = [{"name": "test2", "url": "http://localhost:5000"}, {"name": "test", "url": "http://localhost:5000"}]

    first = True

    text = "{\n"
    for api in apis:
        if not first:
            text = text + ",\n"
        else:
            first = False
        text = text + '\t"' + api["name"] + '": {\n'
        text = text + '\t\t"base": "' + api["url"] + '"\n'
        text = text + "\t}"
        names.append(api["name"])

    text = text + "\n}"

    with open(FILE_PATH, "w") as file:
        file.write(text)

    print("Updated api file. Available apis: " + str(names))


if __name__ == "__main__":
    update()
