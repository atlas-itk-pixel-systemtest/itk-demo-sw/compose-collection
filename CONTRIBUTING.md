# Contribute to `compose-collection`

The default `compose.yaml` file should provide defaults to immediately start the container without further configuration.

The `container_name:` attribute should correspond to the service name to ensure
a singleton container at this level and reachability between containers.

The `default` network should be set to `deminet` so all the containers can talk to each other.

### Roadmap

Add any useful application (and immediately related images like server-client pairs or management UIs) in it's own subdirectory. This is the format expected by eg. Dockge.

We should parametrize all the compose files with the standard DeMi environment variables so we can use them from higher-level DeMi stacks and to configure
multiple instances.


