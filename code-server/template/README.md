# VSCode code-server

## What

This repository contains a maintained `docker compose` file (DeMi "stack") of
[code-server by linuxserver.io](https://docs.linuxserver.io/images/docker-code-server)

Standard scripts to bootstrap Docker and write the required environment variables to `.env` for site-wide shared tools needed by DeMi microservices are provided.


## How

```shell
./bootstrap             # check you have Docker - only the first time
./config                # configure your environment variables
docker compose up -d    # start container
```

if `$CODESERVER_HOSTPORT` is not set the host port is randomly allocated 
(to allow multiple instances, eg. one per user / per site).

In this case bring up the container like this:

```shell
./tasks up
```

This command should print where to access the code-server web ui.

## Configuration

| Variable             | Default                                                   |
| -------------------- | --------------------------------------------------------- |
| CODESERVER_HOSTPORT  | 8443                                                      |
| CODESERVER_WORKSPACE | `$HOME/itk-demo-sw`                                       |
| CODESERVER_IMAGE     | `registry.cern.ch/ghcr.io/linuxserver/code-server:latest` |
| CODESERVER_PASSWORD  | *set in shell before configuring*                         |


---
Contact: G. Brandt <gbrandt@cern.ch>
