import logging
import shutil
import subprocess
from pathlib import Path, PurePath

from python_on_whales import DockerClient, DockerException
from rich.logging import RichHandler

FORMAT = "%(message)s"
logging.basicConfig(
    level="NOTSET", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)
log = logging.getLogger(__name__)

for path in Path.cwd().glob("*"):

    if not path.is_dir() or path.name.startswith('.'):
        continue

    log.info("%s", path)
    try:
        r = subprocess.run(
            ["./config"], 
            capture_output=True,
            cwd=path,
            )
        # log.info(r)
    except FileNotFoundError as exc:
        log.error(exc)
        log.warn("Adding missing config script from template.")
        shutil.copy("./template/config", path)
    except PermissionError as exc:
        log.error(exc)

    
    if not (path / "compose.yaml").is_file():
        log.error(f"No compose.yaml in {path}")
        continue

    try:        
        docker = DockerClient(
            compose_files=[path / "compose.yaml"],
            compose_env_file=PurePath(path).name+"/.env"
        )
        docker.compose.config()
    except DockerException as de:
        # log.exception(de)
        log.error(de)
        # pass

