# Portainer for DeMi

This repository contains a maintained `docker compose` file (DeMi "stack") for Portainer and relevant API scripts to manage DeMi through Portainer.

[Portainer](https://www.portainer.io/) is a special case because is is used to manage all other containers.

Portainer is started separately, and in a system-wide mannner.

Every node can have a Portainer instance running.
For more flexibility, we run Portainer server and agent separately.
This way, all nodes are reachable from each other by adding the respective agents to the servers as Portainer environment.

TO DO:

- check that no two instances run on one node.
- or make configurable to run one Portainer instance per `$DEMI_SITE`.

Portainer use remains optional. You can just use docker compose and/or PyDeMi.

Standard scripts to bootstrap Docker and write the required environment variables to `.env` for site-wide shared tools needed by DeMi microservices are provided.

Usage:

```shell
./bootstrap
./config
docker compose up -d
```

- `bootstrap` this script sets up all dependencies needed to bring up the containers.
- `config` this script writes the `.env` configuration.

<!--
This should be done before starting the tools stack.
The tools stack can then be imported into portainer by 
- uploading `stack.env` to Portainer.
- pointing directly at this Git-repo:
`https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/containers/tools.git`
-->

---
Contact: G. Brandt <gbrandt@cern.ch>
