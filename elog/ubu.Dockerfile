FROM ubuntu
RUN apt update && apt install -y elog && apt clean
RUN chgrp -R 0 /var/lib/elog && \
    chmod -R g=u /var/lib/elog
USER elog
COPY --chown=elog:elog ./elog.conf .
#RUN mkdir -p /usr/share/elog/themes/default/banner
COPY ./elog-banner-css/css/ /usr/share/elog/themes/default/banner

CMD ["elogd", "-v", "-n", "0.0.0.0", "-p", "8080", "-c", "elog.conf"]
