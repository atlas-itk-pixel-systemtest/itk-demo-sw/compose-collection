FROM almalinux

RUN dnf install -y epel-release && \
    dnf install -y emacs-nox \
    ghostscript \
    ImageMagick \
    openssl-devel \
    git gcc gcc-c++ make \
    && yum clean all
# ADD https://elog.psi.ch/elog/download/RPMS/elog-latest.el7.x86_64.rpm /tmp
# RUN dnf -y install /tmp/elog-latest.el7.x86_64.rpm

RUN git clone https://bitbucket.org/ritt/elog --recursive && \
    cd elog && \
    make && \
    make install

#RUN wget https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.11.3/ckeditor_4.11.3_full_easyimage.zip && \
#    unzip ckeditor_4.11.3_full_easyimage.zip && \
#    rm -rf /usr/share/elog/scripts/ckeditor && \
#    mv ckeditor /usr/share/elog/scripts

RUN dnf -y install unzip
ADD https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.23.0-lts/ckeditor_4.23.0-lts_full_easyimage.zip /tmp
RUN unzip /tmp/ckeditor_4.23.0-lts_full_easyimage.zip && \
    ls && \
    mkdir -p /usr/share/elog/scripts && \
    mv ckeditor /usr/share/elog/scripts

# RUN git clone https://github.com/ad3ller/elog-banner-css
RUN mkdir -p /usr/share/elog/themes/default/banner
COPY ./elog-banner-css/css/ /usr/share/elog/themes/default/banner

# TODO (optional): Copy the builder files into /app
#COPY ./elog-src /opt/app-root/

WORKDIR /etc/elog
COPY ./elog.conf ./elog.conf

RUN groupadd elog
RUN useradd -m -r -g elog elog


RUN chown -R elog:elog /etc/elog

# elog logbooks
# RUN mkdir -p /var/lib/elog
# RUN chown -R elog:elog /var/lib/elog

USER elog
CMD ["elogd", "-v", "-p", "8080", "-c", "/etc/elog/elog.conf"]
