FROM almalinux AS builder

RUN dnf install -y rpmdevtools rpmlint \
    gcc-c++ \
    wget \
    openssl-devel \
    which

WORKDIR /root
RUN wget --no-check-certificate https://elog.psi.ch/elog/download/SRPMS/elog-3.1.5-1.el7.src.rpm
# ADD https://elog.psi.ch/elog/download/SRPMS/elog-3.1.5-1.el7.src.rpm .
RUN rpmbuild --rebuild elog-3.1.5-1.el7.src.rpm

# WORKDIR /root/rpmbuild/SPECS/
# RUN rpmbuild -ba elog.spec

RUN rpm -q -l rpmbuild/RPMS/x86_64/elog-3.1.5-1.el9.x86_64.rpm

FROM almalinux

COPY --from=builder --chown=elog:elog /root/rpmbuild/RPMS/x86_64/elog-3.1.5-1.el9.x86_64.rpm .
RUN rpm -q -l elog-3.1.5-1.el9.x86_64.rpm
RUN dnf -y --nogpgcheck localinstall elog-3.1.5-1.el9.x86_64.rpm

# RUN dnf -y install epel-release && dnf -y install ImageMagick unzip

# RUN groupadd elog
# RUN useradd -m -r -g elog elog
# ADD https://download.cksource.com/CKEditor/CKEditor/CKEditor%204.23.0-lts/ckeditor_4.23.0-lts_full_easyimage.zip .
# RUN unzip ckeditor_4.23.0-lts_full_easyimage.zip
# RUN mv ckeditor

# RUN mkdir -p /var/lib/elog
# RUN chgrp -R 0 /var/lib/elog && \
#     chmod -R g=u /var/lib/elog

WORKDIR /elog
COPY --chown=elog:elog ./elogd.cfg .
RUN chown elog:elog -R .

# USER elog
CMD ["elogd", "-v", "-s", "/usr/local/elog", "-n", "0.0.0.0", "-p", "8080", "-c", "elogd.cfg"]
